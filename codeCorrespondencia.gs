var ss = SpreadsheetApp.getActiveSpreadsheet();  
var plantilla = DocumentApp.openByUrl('https://docs.google.com/document/d/1I0wmVz_b0zrh7VqQ_NXSHG-0MQK12yPuxGhG1a3Mk4s/edit');


function onMenu() {
  var ui = SpreadsheetApp.getUi();
  var menu = ui.createMenu('Correspondence');
  menu.addItem('CombinarCorrespondencia', 'option1');
  menu.addToUi();
}

//Funcion evento de la opcion del Menu
function option1(){      
  combinar();
  convertToPDF();
}


function combinar(){  
  
  var rango = ss.getRangeByName("myRange");  
  var data = rango.getValues();
  
  //obtenemos el body de la plantilla
  var body = plantilla.getBody();
  
  //Agregamos una tabla
  var table = body.appendTable();
  
  //Creacion de nueva tabla en el documento
  for(f=0; f<data.length; f++){
    var row = table.appendTableRow();
    for(c=0; c<data[f].length; c++){
      row.appendTableCell(data[f][c]);
    }
  }  
  
  //Ajustes de la tabla  
  table.getRow(0).setBackgroundColor("#0087F4");
  table.setColumnWidth(0, 25);
  table.setColumnWidth(1, 50);
  table.setColumnWidth(2, 80);
  table.setColumnWidth(3, 80);
  table.setColumnWidth(4, 80);
  
  //Guardamos y cerramos la plantilla
  plantilla.saveAndClose();
  
}

function convertToPDF(){  
  
  var plantilla = DriveApp.getFilesByName('Plantilla').next();  
  var pdfDoc = DriveApp.createFile(plantilla.getAs('application/pdf'));
  pdfDoc.setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
  sendEmail(pdfDoc);

}

function sendEmail(pdfDoc){ 
  
  var remitente = Session.getActiveUser().getEmail();  
  var rango = ss.getRangeByName("rangeEmails");  
  var data = rango.getValues();
  
  for(i=0; i<data.length; i++){
    GmailApp.sendEmail(data[i],'Prueba combiacion de correspondencia', 'Este es un ejemplo de Google SpreadSheet con la opción combinar correspondencia ' +  pdfDoc.getUrl());     
  }
}


